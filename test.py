import sys
from steamworks import STEAMWORKS

steamworks = STEAMWORKS()


def get_achievement_list(appid):
    achi_count = steamworks.UserStats.GetNumAchievements()
    achis = []
    for i in range(0, achi_count):
        achi_name = steamworks.UserStats.GetAchievementName(i)
        achis.append(achi_name)
    return achis


def unlock_achievement(ach_names, sync=True):
    for ach_name in ach_names:
        ach_name = ach_name.encode()
        if not steamworks.UserStats.GetAchievement(ach_name):
            steamworks.UserStats.SetAchievement(ach_name)

    if sync:
        store_result = steamworks.UserStats.StoreStats()
        if store_result:
            print("Synced achievements.")
        else:
            print("Had a weird issue syncing achievements.")


def start_game(appid):
    with open("steam_appid.txt", "w") as f:
        f.write(str(appid))

    # Initialize Steam
    steamworks.initialize()

    print(repr(steamworks.UserStats.RequestCurrentStats()))

    achis = get_achievement_list(appid)

    print(repr(achis))

    print(repr(steamworks.UserStats.SetStat(b"loved_gifts", 200)))
    print(repr(steamworks.UserStats.StoreStats()))

    # unlock_achievement(achis)

    input()


start_game(sys.argv[1])
