import os
import sys
from PySide2.QtWidgets import (QApplication, QLabel, QPushButton, QFrame,
                               QListWidget, QLineEdit, QTabWidget,
                               QListWidgetItem, QVBoxLayout, QWidget)
from PySide2.QtCore import Slot, Qt
from steamworks import STEAMWORKS
import json


in_appid = sys.argv[1]

steamworks = STEAMWORKS()


def get_game_name():
    default_name = f"Game #{in_appid}"
    if os.path.exists("ownedgames.json"):
        with open("ownedgames.json") as f:
            ownedgames = json.load(f)
        return ownedgames.get(in_appid, default_name)
    else:
        return default_name


def load_game(appid):
    with open("steam_appid.txt", "w") as f:
        f.write(str(appid))

    # Initialize Steam
    steamworks.initialize()

    # Load game stats
    steamworks.UserStats.RequestCurrentStats()


# hell code
def get_achievement_list(appid):
    achi_count = steamworks.UserStats.GetNumAchievements()
    achis = {}
    for i in range(0, achi_count):
        achi_name_byte = steamworks.UserStats.GetAchievementName(i)
        achi_name = achi_name_byte.decode()
        achi_enabled = steamworks.UserStats.GetAchievement(achi_name_byte)
        achi_dispname = steamworks.UserStats.GetAchievementDisplayAttribute(
            achi_name_byte, b"name").decode()
        achis[achi_name] = {"enabled": achi_enabled,
                            "name": achi_dispname}
    return achis


def unlock_achievement(ach_names, sync=True):
    for ach_name in ach_names:
        ach_name = ach_name.encode()
        if not steamworks.UserStats.GetAchievement(ach_name):
            steamworks.UserStats.SetAchievement(ach_name)

    if sync:
        return steamworks.UserStats.StoreStats()


def lock_achievement(ach_names, sync=True):
    for ach_name in ach_names:
        ach_name = ach_name.encode()
        if steamworks.UserStats.GetAchievement(ach_name):
            steamworks.UserStats.ClearAchievement(ach_name)

    if sync:
        return steamworks.UserStats.StoreStats()


class steam_game(QWidget):
    def initachievementtab(self):
        self.achievementbox = QVBoxLayout()

        self.achievementsframe = QFrame()
        self.achievementsframe.setLayout(self.achievementbox)

        self.achievementslist = QListWidget(self)
        self.achievementbox.addWidget(self.achievementslist)

        achievements = get_achievement_list(in_appid)
        for achievement in achievements:
            achi_name = achievements[achievement]["name"]
            added_entry = QListWidgetItem(achi_name, self.achievementslist)
            added_entry.setData(100, achievement)
            added_entry.setFlags(Qt.ItemFlag.ItemIsEnabled |
                                 Qt.ItemFlag.ItemIsUserCheckable)
            checkstate = (Qt.CheckState.Checked if
                          achievements[achievement]["enabled"] else
                          Qt.CheckState.Unchecked)
            added_entry.setCheckState(checkstate)
            # checkState()

        self.setachis = QPushButton("Save", self)
        self.achievementbox.addWidget(self.setachis)
        self.setachis.clicked.connect(self.set_achievements)

        return self.achievementsframe

    def initstatstab(self):
        self.statsbox = QVBoxLayout()

        self.statsframe = QFrame()
        self.statsframe.setLayout(self.statsbox)

        self.statnamelabel = QLabel("Statistic (API) name:", self)
        self.statsbox.addWidget(self.statnamelabel)

        self.statname = QLineEdit(self)
        self.statsbox.addWidget(self.statname)

        self.getstat = QPushButton("Get", self)
        self.statsbox.addWidget(self.getstat)
        self.getstat.clicked.connect(self.get_stat)

        # TODO: have an int/float selector here, preferably have it be automatic

        self.statvaluelabel = QLabel("Statistic value:", self)
        self.statsbox.addWidget(self.statvaluelabel)

        self.statvalue = QLineEdit(self)
        self.statsbox.addWidget(self.statvalue)

        self.setstat = QPushButton("Set", self)
        self.statsbox.addWidget(self.setstat)
        self.setstat.clicked.connect(self.set_stat)

        return self.statsframe

    def __init__(self):
        QWidget.__init__(self)

        load_game(in_appid)

        self.appbase = QVBoxLayout()

        self.tabwidget = QTabWidget()
        self.appbase.addWidget(self.tabwidget)

        self.achievementstab = self.initachievementtab()
        self.tabwidget.addTab(self.achievementstab, "Achievements")

        self.statstab = self.initstatstab()
        self.tabwidget.addTab(self.statstab, "Stats (WIP)")

        self.setLayout(self.appbase)

    @Slot()
    def get_stat(self):
        val = steamworks.UserStats.GetStatInt(self.statname.text().encode())
        self.statvalue.setText(str(val))

    @Slot()
    def set_stat(self):
        steamworks.UserStats.SetStat(self.statname.text().encode(),
                                     int(self.statvalue.text()))
        steamworks.UserStats.StoreStats()

    @Slot()
    def set_achievements(self):
        for i in range(0, self.achievementslist.count()):
            achi_item = self.achievementslist.item(i)
            achi_name = achi_item.data(100)
            if achi_item.checkState() == Qt.CheckState.Unchecked:
                lock_achievement([achi_name], False)
            else:
                unlock_achievement([achi_name], False)
        steamworks.UserStats.StoreStats()
        return


if __name__ == "__main__":
    app = QApplication(sys.argv)

    widget = steam_game()
    widget.setWindowTitle(f"Linux Achievement Manager: {get_game_name()}")
    widget.resize(800, 600)
    widget.show()

    sys.exit(app.exec_())
