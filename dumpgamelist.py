from steamworks import STEAMWORKS
import requests
import re
import json

game_regex = re.compile(r"<game>([0-9]+)</game>")

app_names = requests.get("http://api.steampowered.com/ISteamApps/GetAppList/v0002/").json()

games_xml = requests.get("http://gib.me/sam/games.xml").text
games = game_regex.findall(games_xml)

steamworks = STEAMWORKS()


def load_game(appid):
    with open("steam_appid.txt", "w") as f:
        f.write(str(appid))

    # Initialize Steam
    steamworks.initialize()


def get_app_name(appid):
    # hellcode
    names = [app["name"] for app in app_names["applist"]["apps"]
             if app["appid"] == appid]
    if names:
        return names[0]
    else:
        return f"Unknown (appid: {appid})"


# Load as spacewar
load_game(480)
owned_games = {}

for game in games:
    game = int(game)
    if steamworks.Apps.IsSubscribedApp(game):
        # TODO: try not to call this over and over again, it's heavy
        app_name = get_app_name(game)
        print(f"Added game #{game} aka {app_name}")
        owned_games[game] = app_name

with open("ownedgames.json", "w") as f:
    json.dump(owned_games, f)
