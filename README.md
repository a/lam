## LAM: Linux Achievement Manager

Basically Steam Achievement Manager, but for Linux.

Should also run on MacOS and Windows as long as you figure out the `LD_LIBRARY_PATH` thing, but I only tested it on Linux.

### How to use (Simple)

Install Python 3.6+ and dependencies on `requirements.txt`, run steam.

[Download the latest release of lam](https://gitlab.com/a/lam/-/releases). Unzip it, run `lam.py` with python3.

First start will be slow as it'll reload the game list. To reload the list manually in the future, run `dumpgamelist.py`.

### How to use (Expert)

Requires Python3.6+ (also deps that are in `requirements.txt`), and https://github.com/aveao/SteamworksPy. Download latest release, put `libsteam_api.so`, `steamworks` (folder) and `SteamworksPy.so` on this folder, then run `lam.py` (ensure that Steam is running while you're doing these).

It'll fetch a list of your games the first time it starts (through `dumpgamelist.py`). To repeat this process, you can delete `ownedgames.json` and restart `lam.py` or run `dumpgamelist.py` manually (with `LD_LIBRARY_PATH="$(pwd)"` before it).

If you prefer skipping the game selector, you can run the game "emulator" and achievement editor directly by running `game.py` with first argument as the app id, and with `LD_LIBRARY_PATH` set to pwd (example: `LD_LIBRARY_PATH="$(pwd)" python3 game.py 480`).

### Beware, ye coward

This is technically a violation of the Steam Subscriber Agreement, don't yell at me if your Steam account gets banned, yadda yadda yadda.

### Screenshots

![Main Page, showing games](https://elixi.re/i/w11guooh.png)

![Game Achievement Entries](https://elixi.re/i/9u0l0dvt.png)

![Game Statistics Manager](https://elixi.re/i/5y9icnlw.png)
