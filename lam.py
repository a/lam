import sys
from PySide2.QtWidgets import (QApplication, QLabel, QListWidget,
                               QListWidgetItem, QVBoxLayout, QWidget)
from PySide2.QtCore import Slot
import subprocess
import os
import json


def get_games():
    if not os.path.exists("ownedgames.json"):
        dumper_path = os.path.join(os.getcwd(), "dumpgamelist.py")
        cmd = f"LD_LIBRARY_PATH=\"{os.getcwd()}\" "\
              f"{sys.executable} {dumper_path}"

        subprocess.run(cmd, shell=True)

    with open("ownedgames.json") as f:
        return json.load(f)


class lam(QWidget):
    def initgamelist(self):
        self.infolabel = QLabel("Welcome to Linux Achievement Manager! "
                                "(https://gitlab.com/a/lam) "
                                "Double click on a game to start.", self)
        self.appbase.addWidget(self.infolabel)

        self.gameslist = QListWidget(self)
        self.gameslist.setSortingEnabled(True)
        self.appbase.addWidget(self.gameslist)

        games = get_games()
        for game in games:
            game_name = games[game]
            added_entry = QListWidgetItem(game_name, self.gameslist)
            added_entry.setData(100, game)

        self.gameslist.itemActivated.connect(self.load_game)

    def __init__(self):
        QWidget.__init__(self)

        self.appbase = QVBoxLayout()
        self.initgamelist()

        self.setLayout(self.appbase)

    @Slot()
    def load_game(self, widget):
        appid = widget.data(100)
        game_path = os.path.join(os.getcwd(), "game.py")
        cmd = f"LD_LIBRARY_PATH=\"{os.getcwd()}\" "\
              f"{sys.executable} {game_path} {appid}"

        # Hide main windows and show second one
        self.hide()
        subprocess.run(cmd, shell=True)
        self.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)

    widget = lam()
    widget.setWindowTitle("Linux Achievement Manager")
    widget.resize(800, 600)
    widget.show()

    sys.exit(app.exec_())
